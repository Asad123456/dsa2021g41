﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectFinal
{
    class Validation
    {
        public bool isValidEmail(string email)
        {
            bool flag = false;
            int size = email.Length;
            if (!((email[0] == '@') || (email[0] == '.') || (email[0] >= '0' && email[0] <= '9')))
            {
                for (int i = 0; i < size; i++)
                {
                    if ((email[i] >= 'A' && email[i] <= 'Z') || (email[i] >= 'a' && email[i] <= 'z') || email[i] == '@' || email[i] == '.' || (email[i] >= '0' && email[i] <= '9'))
                    {
                        flag = true;
                    }
                    else
                    {
                        MessageBox.Show("Email is not Valid");
                        flag = false;
                        break;
                    }

                }
            }
            else
            {
                MessageBox.Show("Email is not Valid");
                flag = false;
            }
            return flag;
        }
    }
}

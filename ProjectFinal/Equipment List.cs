﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectFinal
{
    public partial class Equipment_List : Form
    {
        public Equipment_List()
        {
            InitializeComponent();
            LoadData();
            LoadIntoTable();
        }
        Ambulance_Service_System amb = Ambulance_Service_System.getInstance();
        Admin ad = new Admin();

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MedicalSupplements M = new MedicalSupplements();
            M.name = textBox1.Text;
            M.quantity = int.Parse(textBox2.Text);
            amb.addMedicalSupplements(M);
            amb.AddMedicalRequiredRecord(M);
            LoadIntoTable();
            setClear();

        }
        public void LoadData()
        {
            try
            {
                using (var f = new StreamReader(@"c:\\users\\khadija\\desktop\\dsa2021g41\\files\\EquipmentList.txt"))
                {
                    string line = string.Empty;
                    while ((line = f.ReadLine()) != null)
                    {
                        MedicalSupplements M = new MedicalSupplements();
                        var s = line.Split(',');
                        M.name = s[0];
                        M.quantity = int.Parse(s[1]);
                        amb.addMedicalSupplements(M);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
            }
        }
        public void LoadIntoTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name"));
            dt.Columns.Add(new DataColumn("Quantity"));
            ad.List = amb.returnMedicalList();
            for (int i = 0; i < ad.List.Count;i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = ad.List[i].name;
                dr[1] = ad.List[i].quantity;
                dt.Rows.Add(dr);
                dataGridView1.DataSource = dt;
            }

        }
        private void setClear()
        {
            textBox1.ResetText();
            textBox2.ResetText();
        }


        private void Equipment_List_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    } 
    
}

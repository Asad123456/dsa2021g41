﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectFinal
{
    class Vehicle
    {
        private string Type;
        private string number;
        private int numberOfPeoplesAllowed;
        private int ParkingSlotNumber;

        public string typeofVehicle
        {
            get { return Type; }
            set { this.Type = value; }
        }
        public string Number
        {
            get { return number; }
            set { this.number = value; }
        }
        public int peoplesAllowed
        {
            get { return numberOfPeoplesAllowed; }
            set { this.numberOfPeoplesAllowed = value; }
        }
        public int slotNumber
        {
            get { return this.ParkingSlotNumber; }
            set { this.ParkingSlotNumber = value; }
        }

        public virtual string getType()
        {
            return "Vehicle";
        }
    }
}

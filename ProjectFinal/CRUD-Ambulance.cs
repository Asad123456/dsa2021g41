﻿using ProjectFinal.Data_Structures;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace ProjectFinal
{
    public partial class CRUD_Ambulance : Form
    {

        Ambulance_Service_System amb = Ambulance_Service_System.getInstance();
        LinkedList l = new LinkedList();
        int count = 0;
        



        public CRUD_Ambulance()
        {
            InitializeComponent();
            loaddat();
            LoadIntoTable();

        }
        DataTable T = new DataTable();
        int indexRow;
        string num;
        private void setClear()
        {
            textBox1.ResetText();
            textBox2.ResetText();
            textBox3.ResetText();
            textBox4.ResetText();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Vehicle v1 = new Vehicle();
            v1.typeofVehicle = textBox1.Text;
            v1.Number = textBox2.Text;
            v1.slotNumber = int.Parse(textBox4.Text);
            v1.peoplesAllowed = int.Parse(textBox3.Text);
            l.insertAmb(v1);
            amb.addRecord(v1);
            LoadIntoTable();
            setClear();

        }
        public void LoadIntoTable()
        {
            Node n = new Node();
            n = l.head;
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Type of Ambulance"));
            dt.Columns.Add(new DataColumn("Number"));
            dt.Columns.Add(new DataColumn("Parking Slot Numbers"));
            dt.Columns.Add(new DataColumn("Number of Peoples Allowed"));
            while (n != null)
            {
                DataRow dr = dt.NewRow();
                dr[0] = n.Data.typeofVehicle;
                dr[1] = n.Data.Number;
                dr[2] = n.Data.slotNumber;
                dr[3] = n.Data.peoplesAllowed;
                dt.Rows.Add(dr);
                dataGridView1.DataSource = dt;
                n = n.Next;
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void CRUD_Ambulance_Load(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
        public void loaddat()
        {
            try
            {

                using (var f = new StreamReader(@"c:\\users\\khadija\\desktop\\dsa2021g41\\files\\ambulancerecord.txt"))
                {
                    string line = string.Empty;
                    while ((line = f.ReadLine()) != null)
                    {
                        Vehicle v = new Vehicle();
                        var s = line.Split(',');
                        v.typeofVehicle = s[0];
                        v.Number = s[1];
                        v.slotNumber = int.Parse(s[2]);
                        v.peoplesAllowed = int.Parse(s[3]);
                        count++;
                        l.insertAmb(v);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
            textBox2.Text = row.Cells[1].Value.ToString();
            textBox4.Text = row.Cells[2].Value.ToString();
            textBox3.Text = row.Cells[3].Value.ToString();
            num = textBox2.Text;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Vehicle v = new Vehicle();
            DataGridViewRow dat = dataGridView1.Rows[indexRow];
            dat.Cells[0].Value = textBox1.Text;
            dat.Cells[1].Value = textBox2.Text;
            dat.Cells[2].Value = textBox4.Text;
            dat.Cells[3].Value = textBox3.Text;
            v.typeofVehicle = textBox1.Text;
            v.Number = textBox2.Text;
            v.slotNumber = int.Parse(textBox4.Text);
            v.peoplesAllowed = int.Parse(textBox3.Text);
            l.UpdateAmbulanceRecord(num, v);
            l.printAmbRecord();
            LoadIntoTable();
            setClear();
            amb.ClearFile();
            Node n = new Node();
            n = l.head;
            while (n != null)
            {
                Vehicle V = new Vehicle();
                V.typeofVehicle = n.Data.typeofVehicle;
                V.Number = n.Data.Number;
                V.slotNumber = n.Data.slotNumber;
                V.peoplesAllowed = n.Data.peoplesAllowed;
                amb.addRecord(V);
                n = n.Next;
            }
            loaddat();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int row = dataGridView1.CurrentCell.RowIndex;
            DataGridViewRow dat = dataGridView1.Rows[row];
            string x = dat.Cells[1].Value.ToString();
            l.DeleteAmbRecord(x);
            MessageBox.Show("Node deleted successfully");
            l.printAmbRecord();
            dataGridView1.Rows.RemoveAt(row);
            amb.ClearFile();
            Node n = new Node();
            n = l.head;
            while (n != null)
            {
                MessageBox.Show("Accessing list instance : "+n.Data.Number);
                Vehicle V = new Vehicle();
                V.typeofVehicle = n.Data.typeofVehicle;
                V.Number = n.Data.Number;
                V.slotNumber = n.Data.slotNumber;
                V.peoplesAllowed = n.Data.peoplesAllowed;
                amb.addRecord(V);
                n = n.Next;
            }
            loaddat();
        }
       
    }

}

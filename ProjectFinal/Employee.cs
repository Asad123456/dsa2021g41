﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectFinal
{
    class Employee:Person
    {
        private string name;
        private string AssignedAmbulanceSlotNumber;
        private int peoplesAllowed;
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string AmbNumber
        {
            get { return this.AssignedAmbulanceSlotNumber; }
            set { this.AssignedAmbulanceSlotNumber = value; }
        }
        public int allowedPeople
        {
           get{ return this.peoplesAllowed; }
           set { this.peoplesAllowed = value; }
        }
    }
}

﻿using System;
using ProjectFinal.Data_Structures;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProjectFinal
{

    public partial class EquipmentList_Procurement_ : Form
    {
        Ambulance_Service_System amb = Ambulance_Service_System.getInstance();
        ProcurementDepartmentManager pr = new ProcurementDepartmentManager();

        public EquipmentList_Procurement_()
        {
            InitializeComponent();
            loaddat();
            LoadIntoTable();
            //amb.Bst.printInordr(amb.Bst.root);
        }


        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            EquipmentList_Assign_ F = new EquipmentList_Assign_();
            F.Show();
        }
        public void loaddat()
        {
            try
            {

                using (var f = new StreamReader(@"c:\\users\\khadija\\desktop\\dsa2021g41\\files\\AvailableMedicalInStock.txt"))
                {
                    string line = string.Empty;
                    while ((line = f.ReadLine()) != null)
                    {
                        ProcurementDepartmentManager P = new ProcurementDepartmentManager();
                        var s = line.Split(',');
                        P.M.name = s[0];
                        P.M.quantity = int.Parse(s[1]);
                        P.M.exp = s[2];
                        P.M.mfg = s[3];
                        amb.addMedList(P);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
            }
        }
        public void LoadIntoTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name"));
            dt.Columns.Add(new DataColumn("Quantity"));
            dt.Columns.Add(new DataColumn("Expiry Date"));
            dt.Columns.Add(new DataColumn("Manfacturing Date"));
            if (amb.Bst.root != null)
            {
                DataRow dr = dt.NewRow();
                dr[0] = amb.Bst.root.medData.name;
                dr[1] = amb.Bst.root.medData.quantity;
                dr[2] = amb.Bst.root.medData.exp;
                dr[3] = amb.Bst.root.medData.mfg;
                dt.Rows.Add(dr);
                dataGridView1.DataSource = dt;
            }


        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectFinal.Data_Structures
{
    class Node
    {
        public MedicalSupplements medData{ get; set; }
        public Node right { get; set; }
        public Node Left { get; set; }
        public Vehicle Data { get; set; }
        public Person personData { get; set; }
        public Node Next { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectFinal
{
    public partial class UserLoginScreen : Form
    {
        Ambulance_Service_System amb = Ambulance_Service_System.getInstance();
        public UserLoginScreen()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Person p = new Person();
            p = new User();
            ((User)p).Name = textBox2.Text;
            if (radioButton1.Checked)
            {
                ((User)p).Condition = radioButton1.Text;
            }
            if (radioButton2.Checked)
            {
                ((User)p).Condition = radioButton2.Text;
            }
            if (radioButton3.Checked)
            {
                ((User)p).Condition = radioButton3.Text;
            }
            ((User)p).location = textBox3.Text;
            p.Email = textBox1.Text;

            Validation V = new Validation();
            if (V.isValidEmail(textBox1.Text))
            {
                
                amb.add(p);
                amb.print();
                LocationAndTracking L1 = new LocationAndTracking();
                L1.Show();
                this.Hide();
            }
            amb.addUserRecord(p);
        }
        
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectFinal
{
    class User:Person
    {
        private string Location;
        private string condition;
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string location
        {
            get { return Location; }
            set { Location = value; }
        }
        public string Condition
        {
            get { return condition; }
            set { condition = value; }
        }

        public override string getType()
        {
            return "User";
        }
    }
}

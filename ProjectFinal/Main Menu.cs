﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectFinal
{
    public partial class Main_Menu : Form
    {
        public Main_Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1();
            f1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ManagerLoginScreen M1 = new ManagerLoginScreen();
            M1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            UserLoginScreen U1 = new UserLoginScreen();
            U1.Show();
        }
    }
}

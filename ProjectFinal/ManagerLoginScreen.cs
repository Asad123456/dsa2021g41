﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectFinal
{
    public partial class ManagerLoginScreen : Form
    {
        public ManagerLoginScreen()
        {
            InitializeComponent();
        }

        private void ManagerLoginScreen_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Person p = new Person();
            p.Email = textBox1.Text;
            p.Password = textBox2.Text;
            if(p.Email.Equals("manager") && p.Password.Equals("manager123"))
            {
                EquipmentList_Procurement_ equip = new EquipmentList_Procurement_();
                equip.Show();
                this.Hide();
            }
        }
    }
}
